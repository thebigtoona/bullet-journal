const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const cors = require('cors')
const passport = require('passport')
const mongoose = require('mongoose')
const config = require('./config/database')

// connect to db 
mongoose.connect(config.database)

// on db connect 
mongoose.connection.on('conneced', () => {
    console.log('DATABASE CONNECTED: ' + config.database)
})

// on db error 
mongoose.connection.on('error', (err) => {
    console.log('Database ERROR:  ' + err)
})

// init express 
const app = express()

const users = require('./routes/users')

// port variable
const port = 4000

// set up cors middleware 
app.use(cors())

// set path for client 
app.use(express.static(path.join(__dirname, 'client')))

// set up body parser middleware  
app.use(bodyParser.json())

// passport middleware
app.use(passport.initialize())
app.use(passport.session())

require('./config/passport')(passport);

app.use('/users', users)

// route settings 
app.get('/', (req, res) => res.send('Invalid Endpoint'))


// tell app to listen on the port 
app.listen(port, () => console.log('Server started on port ' + port) )
process.env.NODE_ENV = 'test'

const mongoose = require('mongoose')
const User = require('../models/user')

//Require the dev-dependencies
const chai = require('chai')
const chaiHttp = require('chai-http')
// const server = require('../app.js')
const should = chai.should()

chai.use(chaiHttp)

describe('User', () => {
    
    // beforeEach(done => {
    //     User.remove( {}, err => {
    //         done();
    //     })
    // })

    describe('/POST register', () => {
        it('should register a user', done => {
            console.log('this test needs to be built')
            done()
        })
    })

    describe('/POST authenticate', () => {
        it('should authenticate a user for login', done => {
            console.log('this test needs to be built')
            done()
        })
    })

    describe('/GET profile', () => {
        it('should send back the user information using a token', done => {
            console.log('this test needs to be built')
            done()
        })
        it('should return unauthorized if a token is not supplied', done => {
            console.log('this test needs to be built')
            done()
        })
    })
})